﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Transactions;

namespace SafeSynTools
{
    public partial class Form1 : Form
    {
        int second_time = Convert.ToInt32(ConfigurationManager.AppSettings["seconds_time"]);//获取实时同步的执行间隔时间
        string moveIdentityid = ConfigurationManager.AppSettings["moveIdentityid"].ToString();
        string reportConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_report"].ToString();//report
        string mainConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_main"].ToString();//main
        string sql_share = ConfigurationManager.ConnectionStrings["sqlConnectionString_share"].ToString();
        string sql_agentOut = ConfigurationManager.ConnectionStrings["sqlConnectionString_agentOut"].ToString();
        string sql_totalOut = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalOut"].ToString();
        string errorLogPath = System.Environment.CurrentDirectory + "\\errorlog.txt";//错误日志记录路径
        string successLogPath = System.Environment.CurrentDirectory + "\\successlog.txt";//成功日志记录路径
        System.Timers.Timer timer_clear = new System.Timers.Timer();
        System.Timers.Timer compareDelayTimer = new System.Timers.Timer();
        bool reportDelay = false;

        public Form1()
        {
            InitializeComponent();
            second_time = second_time * 1000;//秒
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            //创建实时同步的线程
            Thread thread = new Thread(new ThreadStart(syn_proxySum_test));
            thread.Start();
            timer_clear.AutoReset = false;
            timer_clear.Interval = 10000;
            timer_clear.Elapsed += new System.Timers.ElapsedEventHandler(clear_Elapse);
            timer_clear.Start();

            compareDelayTimer.Interval = 10000;
            compareDelayTimer.AutoReset = false;
            compareDelayTimer.Elapsed += new System.Timers.ElapsedEventHandler(compareSQLTime_Elapsed);
            compareDelayTimer.Start();
        }

        #region 实时同步的方法
        void syn_proxySum_test()
        {
            string tableString = ConfigurationManager.AppSettings["onlyOne_test"].ToString();
            if (string.IsNullOrEmpty(tableString) == false)
            {
                FillMsg1("正在同步...");
                string[] tablenames = tableString.Split(',');
                //string date_now = "";
                int count = 0;
                while (true)
                {
                    //    date_now = System.DateTime.Now.ToString("yyyy-MM-dd");
                    //    if (true)dt_users_class_verify_cold() == 0
                    //    {
                    //        //每日迴圈
                    //        while (date_now == System.DateTime.Now.ToString("yyyy-MM-dd"))
                    //        {
                            if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                            {
                                Thread.Sleep(1000 * 60 * 60 * 3);
                                continue;
                            }
                            //if (dt_users_class_verify() == 0)
                            //{
                                count = 0;
                                foreach (var tablename in tablenames)
                                {
                                    try
                                    {
                                        //string querystr = string.Format("select lockid from dt_tablelockid_test where tableName='{0}'", tablename);
                                        //DataTable locktable; 
                                        //if (tablename == "dt_user_get_point_record")
                                        //{
                                        //    locktable = SqlDbHelper.GetQuery(querystr, sql_totalOut);
                                        //}
                                        //else
                                        //{
                                        //    locktable = SqlDbHelper.GetQuery(querystr, sql_agentOut);
                                        //}
                                        //List<string> maxLockid_list = new List<string>();
                                        //byte[] lockid = StringConvertByte(locktable.Rows[0]["lockid"].ToString());
                                        List<SqlParameter> AzureSqlParamter = new List<SqlParameter>()
                                        {
                                            //new SqlParameter("@lockid", SqlDbType.Timestamp){ Value=lockid},
                                            new SqlParameter("@flog1", SqlDbType.Int){ Value=1},
                                            new SqlParameter("@state1", SqlDbType.Int){ Value=1},
                                            new SqlParameter("@state3", SqlDbType.Int){ Value=3},
                                            new SqlParameter("@type3", SqlDbType.Int){ Value=3},
                                            new SqlParameter("@type9", SqlDbType.Int){ Value=9},
                                            new SqlParameter("@type10", SqlDbType.Int){ Value=10},
                                            new SqlParameter("@openstate1", SqlDbType.Int){ Value=1},
                                            new SqlParameter("@openstate2", SqlDbType.Int){ Value=2}
                                        };
                                        string columns = "*";
                                        if (tablename == "dt_user_in_account")
                                            columns = "id,identityid,user_id,money,type,type2,state,add_time,cztime,add_time2,SourceName";
                                        else if (tablename == "dt_user_out_account")
                                            columns = "id,identityid,user_id,money,type,type2,state,add_time,cztime,SourceName";
                                        else if (tablename == "dt_lottery_orders")
                                            columns = "id,identityid,user_id,lottery_code,normal_money,add_time,SourceName";
                                        else if (tablename == "dt_lottery_winning_record")
                                            columns = "id,identityid,user_id,lottery_code,bonus_money,add_time,SourceName";
                                        else
                                            columns = "id,identityid,user_id,point_cash,lotterycode,add_time,SourceName";

                                        string str = "select top 1000 " + columns + " from " + tablename + " where lockid>@lockid and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") ;
                                        if (tablename == "dt_user_in_account")
                                            str = "select top 1000 id,identityid,user_id,money,type,type2,state,add_time,cztime,add_time2,SourceName from dt_user_in_account with(index(index_reportSyn)) where reportSyn=1 and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from dt_users where flog=@flog1) order by id";
                                            //str = "select top 1000 id,identityid,user_id,money,type,type2,state,add_time,cztime,add_time2,SourceName,lockid from dt_user_in_account where lockid>@lockid and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "type in(1,7,11,12,17,30) and state=1 and user_id in(select id from dt_users where flog=@flog1) order by lockid asc";
                                        else if (tablename == "dt_user_out_account")
                                            str = "select top 1000 id,identityid,user_id,money,type,type2,state,add_time,cztime,SourceName from dt_user_out_account with(index(index_reportSyn)) where reportSyn=1 and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from dt_users where flog=@flog1) order by id";
                                        else if (tablename == "dt_lottery_orders")
                                            str = "select top 1000 id,identityid,user_id,lottery_code,normal_money,add_time,SourceName from " + tablename + " with(index(index_reportSyn)) where reportSyn=1 and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "flog=@flog1 order by add_time";
                                            //str = str + "flog=@flog1 AND openstate in(@openstate1,@openstate2) order by lockid asc";
                                        else if (tablename == "dt_lottery_winning_record")
                                            str = "select top 1000 id,identityid,user_id,lottery_code,bonus_money,add_time,SourceName from " + tablename + " with(index(index_reportSyn)) where reportSyn=0 and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "flog=@flog1 order by id";
                                            //str = str + "flog=@flog1 order by lockid asc";
                                        else if (tablename == "dt_user_get_point_record")
                                            str = "select top 1000 id,identityid,user_id,point_cash,lotterycode,add_time,SourceName from " + tablename + " with(index(index_reportSyn)) where reportSyn=0 and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from dt_users where flog=@flog1) order by id";

                                        var table = SqlDbHelper.GetQuery(str, AzureSqlParamter.ToArray(), (reportDelay == true ? mainConnStr : reportConnStr));
                                        if (table == null)
                                            continue;
                                        count = table.Rows.Count;
                                        if (table.Rows.Count != 0)//有数据时更新到本地数据库中
                                        {
                                            string maxlockid = "isVerify";
                                            //string idStr = "";
                                            List<SqlParameter> SqlParamter = new List<SqlParameter>();
                                            //if (tablename == "dt_lottery_orders" || tablename == "dt_lottery_winning_record" || tablename == "dt_user_get_point_record" || tablename == "dt_user_out_account")
                                            //{
                                            //    //for (int r = 0; r < table.Rows.Count; r++)
                                            //    //{
                                            //    //    idStr += "@id" + (r + 1) + ",";
                                            //    //    SqlParamter.Add(new SqlParameter("@id" + (r + 1), table.Rows[r]["id"].ToString().TrimEnd()));
                                            //    //}
                                            //    //idStr = idStr.Substring(0, idStr.Length - 1);
                                            //}
                                            //else
                                            //{
                                            //    //将最大lockid转换成字符串
                                            //    byte[] a = (byte[])table.Rows[table.Rows.Count - 1]["lockid"];
                                            //    maxlockid = "0x" + BitConverter.ToString(a).Replace("-", "");
                                            //    table.Columns.Remove("lockid");
                                            //}


                                            int resultA = 0;
                                            //if (tablename != "dt_user_get_point_record")
                                            //{
                                                TimeSpan tsA1 = new TimeSpan(DateTime.Now.Ticks);
                                                if (tablename == "dt_user_in_account")
                                                    resultA = SqlDbHelper.RunInsert(table, maxlockid, "dsp_user_in_proxySum_test", sql_agentOut);
                                                else if (tablename == "dt_user_out_account")
                                                    resultA = SqlDbHelper.RunInsert(table, maxlockid, "dsp_user_out_proxySum_test", sql_agentOut);
                                                else if (tablename == "dt_lottery_orders")
                                                    resultA = SqlDbHelper.RunInsert(table, maxlockid, "dsp_lottery_proxySum_self_test", sql_agentOut);
                                                else if (tablename == "dt_lottery_winning_record")
                                                    resultA = SqlDbHelper.RunInsert(table, maxlockid, "dsp_lottery_winning_proxySum_test", sql_agentOut);
                                                else
                                                    resultA = SqlDbHelper.RunInsert(table, maxlockid, "dsp_user_get_point_proxySum_test", sql_agentOut);
                                                TimeSpan tsA2 = new TimeSpan(DateTime.Now.Ticks);
                                                TimeSpan tsA = tsA1.Subtract(tsA2).Duration();
                                                string dateDiffA = tsA.Seconds.ToString() + "秒";
                                                if (tablename == "dt_user_in_account" || tablename == "dt_user_out_account")
                                                    FillMsg1("Agent " + tablename + "成功汇总" + count + "条数据,耗时:" + dateDiffA);
                                                else
                                                    FillMsg2("Agent " + tablename + "成功汇总" + count + "条数据,耗时:" + dateDiffA);
                                                WriteLogData("Agent " + tablename + "成功汇总" + count + "条数据,耗时:" + dateDiffA + "    同步汇总时间:" + DateTime.Now.ToString());
                                            //}
                                            //一樣的資料送到TotalOut
                                            TimeSpan tsT1 = new TimeSpan(DateTime.Now.Ticks);
                                            int resultT = 0;
                                            if (tablename == "dt_user_in_account")
                                                resultT = SqlDbHelper.RunInsert(table, maxlockid, "dsp_user_in_account_synSum_test", sql_totalOut);
                                            else if (tablename == "dt_user_out_account")
                                                resultT = SqlDbHelper.RunInsert(table, maxlockid, "dsp_user_out_account_synSum_test", sql_totalOut);
                                            else if (tablename == "dt_lottery_orders")
                                                resultT = SqlDbHelper.RunInsert(table, maxlockid, "dsp_lottery_orders_synSum_test", sql_totalOut);
                                            else if (tablename == "dt_lottery_winning_record")
                                                resultT = SqlDbHelper.RunInsert(table, maxlockid, "dsp_lottery_winning_synSum_test", sql_totalOut);
                                            else if (tablename == "dt_user_get_point_record")
                                                resultT = SqlDbHelper.RunInsert(table, maxlockid, "dsp_user_get_point_synSum_test", sql_totalOut);
                                            TimeSpan tsT2 = new TimeSpan(DateTime.Now.Ticks);
                                            TimeSpan tsT = tsT1.Subtract(tsT2).Duration();
                                            string dateDiffT = tsT.Seconds.ToString() + "秒";
                                            if (tablename == "dt_user_in_account" || tablename == "dt_user_out_account")
                                                FillMsg1("Total " + tablename + "成功汇总" + count + "条数据,耗时:" + dateDiffT);
                                            else
                                                FillMsg2("Total " + tablename + "成功汇总" + count + "条数据,耗时:" + dateDiffT);
                                            WriteLogData("Total " + tablename + "成功汇总" + count + "条数据,耗时:" + dateDiffT + "    同步汇总时间:" + DateTime.Now.ToString());

                                            if (resultA == 0 && resultT == 0 )
                                            {
                                                TimeSpan tsU1 = new TimeSpan(DateTime.Now.Ticks);
                                                if (tablename == "dt_lottery_orders")
                                                {
                                                    table.Columns.Remove("identityid");
                                                    table.Columns.Remove("user_id");
                                                    table.Columns.Remove("lottery_code");
                                                    table.Columns.Remove("normal_money");
                                                    table.Columns.Remove("add_time");
                                                    table.Columns.Remove("SourceName");
                                                    SqlParameter Parameter_id = new SqlParameter("@idTable", SqlDbType.Structured);
                                                    Parameter_id.Value = table;
                                                    Parameter_id.TypeName = "report_id_type";
                                                    SqlParamter.Add(Parameter_id);

                                                    SqlDbHelper.ExecuteNonQuery("update " + tablename + " set reportSyn=2 where reportSyn=1 and id in (select id from @idTable)", SqlParamter.ToArray(), mainConnStr);
                                                }
                                                else if (tablename == "dt_lottery_winning_record")
                                                {
                                                    table.Columns.Remove("identityid");
                                                    table.Columns.Remove("user_id");
                                                    table.Columns.Remove("lottery_code");
                                                    table.Columns.Remove("bonus_money");
                                                    table.Columns.Remove("add_time");
                                                    table.Columns.Remove("SourceName");
                                                    SqlParameter Parameter_id = new SqlParameter("@idTable", SqlDbType.Structured);
                                                    Parameter_id.Value = table;
                                                    Parameter_id.TypeName = "report_id_type";
                                                    SqlParamter.Add(Parameter_id);

                                                    SqlDbHelper.ExecuteNonQuery("update " + tablename + " set reportSyn=2 where id in (select id from @idTable)", SqlParamter.ToArray(), mainConnStr);
                                                }
                                                else if (tablename == "dt_user_in_account")
                                                {
                                                    table.Columns.Remove("identityid");
                                                    table.Columns.Remove("user_id");
                                                    table.Columns.Remove("money");
                                                    table.Columns.Remove("type");
                                                    table.Columns.Remove("type2");
                                                    table.Columns.Remove("state");
                                                    table.Columns.Remove("add_time");
                                                    table.Columns.Remove("cztime");
                                                    table.Columns.Remove("add_time2");
                                                    table.Columns.Remove("SourceName");
                                                    SqlParameter Parameter_id = new SqlParameter("@idTable", SqlDbType.Structured);
                                                    Parameter_id.Value = table;
                                                    Parameter_id.TypeName = "report_id_type";
                                                    SqlParamter.Add(Parameter_id);

                                                    SqlDbHelper.ExecuteNonQuery("update " + tablename + " set reportSyn=2 where id in (select id from @idTable)", SqlParamter.ToArray(), mainConnStr);
                                                }
                                                else if (tablename == "dt_user_out_account")
                                                {
                                                    table.Columns.Remove("identityid");
                                                    table.Columns.Remove("user_id");
                                                    table.Columns.Remove("money");
                                                    table.Columns.Remove("type");
                                                    table.Columns.Remove("type2");
                                                    table.Columns.Remove("state");
                                                    table.Columns.Remove("add_time");
                                                    table.Columns.Remove("cztime");
                                                    table.Columns.Remove("SourceName");
                                                    SqlParameter Parameter_id = new SqlParameter("@idTable", SqlDbType.Structured);
                                                    Parameter_id.Value = table;
                                                    Parameter_id.TypeName = "report_id_type";
                                                    SqlParamter.Add(Parameter_id);

                                                    SqlDbHelper.ExecuteNonQuery("update " + tablename + " set reportSyn=2 where id in (select id from @idTable)", SqlParamter.ToArray(), mainConnStr);
                                                }
                                                else
                                                {
                                                    table.Columns.Remove("identityid");
                                                    table.Columns.Remove("user_id");
                                                    table.Columns.Remove("point_cash");
                                                    table.Columns.Remove("lotterycode");
                                                    table.Columns.Remove("add_time");
                                                    table.Columns.Remove("SourceName");
                                                    SqlParameter Parameter_id = new SqlParameter("@idTable", SqlDbType.Structured);
                                                    Parameter_id.Value = table;
                                                    Parameter_id.TypeName = "report_id_type";
                                                    SqlParamter.Add(Parameter_id);

                                                    SqlDbHelper.ExecuteNonQuery("update " + tablename + " set reportSyn=2 from dt_user_get_point_record with(index(index_id)) where id in (select id from @idTable)", SqlParamter.ToArray(), mainConnStr);
                                                }
                                                TimeSpan tsU2 = new TimeSpan(DateTime.Now.Ticks);
                                                TimeSpan tsU = tsU1.Subtract(tsU2).Duration();
                                                string dateDiffU = tsU.Seconds.ToString() + "秒";
                                                WriteLogData("主表 結束更新" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fffffff"));
                                                FillMsg2("成功更新主表" + count + "条数据,耗时:" + dateDiffU);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        if (ex.Message != "未将对象引用设置到对象的实例。")
                                        {
                                            FillErrorMsg(tablename + ":" + ex);
                                            WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                                        }
                                    }
                                }
                                Thread.Sleep(second_time);//睡眠时间
                            //}
                //        }
                //    }
                }
            }
        }

        /// <summary>
        /// 代理汇总前先判断下层级表的准确性 昨天前
        /// </summary>
        int dt_users_class_verify_cold()
        {
            int result = 0;
            int count = 0;
            int reportcount = 0;
            int azurecount = 0;
            int reportmax = 0;
            int azuremax = 0;
            try
            {
                //int xx = Convert.ToInt32("sss0d5fg4");
                string date_now = System.DateTime.Now.ToString("yyyy-MM-dd");
                //比對每日的數量及最大最小值
                List<SqlParameter> LocalSqlParamter = new List<SqlParameter>()
                {
                    new SqlParameter("@date_now",SqlDbType.Date){ Value=date_now },
                    new SqlParameter("@flog1", SqlDbType.Int){ Value=1}
                };
                string str_report = "select convert(char(10),addtime,121) addtime,count(id) count,isnull(min(id),0) min,isnull(max(id),0) max from dt_users_class_test where " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "addtime < @date_now group by convert(char(10),addtime,121) order by convert(char(10),addtime,121)";
                string str_congku = "select convert(char(10),addtime,121) addtime,count(id) count,isnull(min(id),0) min,isnull(max(id),0) max from dt_users_class where " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from dt_users where flog=@flog1) and addtime < @date_now group by convert(char(10),addtime,121) order by convert(char(10),addtime,121)";
                //获取报表中的层级最新的id
                var reporttable = SqlDbHelper.GetQuery(str_report, LocalSqlParamter.ToArray(), sql_agentOut);
                //获取从库中层级最新的id
                var azurettable = SqlDbHelper.GetQuery(str_congku, LocalSqlParamter.ToArray(),reportConnStr);
                TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                for (int i = 0; i < azurettable.Rows.Count; i++)
                {
                    string date_str = azurettable.Rows[i]["addtime"].ToString().TrimEnd();
                    string date_str_e = Convert.ToDateTime(azurettable.Rows[i]["addtime"]).AddDays(1).ToString("yyyy-MM-dd");
                    azurecount = Convert.ToInt32(azurettable.Rows[i]["count"]);
                    azuremax = Convert.ToInt32(azurettable.Rows[i]["max"]);

                    if (reporttable.Select("addtime ='" + date_str + "'").Count() > 0)
                    {
                        reportmax = Convert.ToInt32(reporttable.Select("addtime ='" + date_str + "'")[0]["max"]);
                        reportcount = Convert.ToInt32(reporttable.Select("addtime ='" + date_str + "'")[0]["count"]);
                        if (azurecount > reportcount || azuremax > reportmax)//如報表有叢庫當天資料,並且報表數量和maxid都小於叢庫
                        {
                            count = Crue_data(azurecount, reportcount, azuremax, reportmax, date_str, date_str_e);
                        }
                    }
                    else //如果報表沒叢庫當天數據則補數據
                    {
                        count = Crue_data(azurecount, reportcount, azuremax, reportmax, date_str, date_str_e);
                    }
                }

                TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                TimeSpan ts = ts1.Subtract(ts2).Duration();
                string dateDiff = ts.Seconds.ToString() + "秒";
                FillMsg3("dt_user_class成功同步汇总" + count + "条数据,耗时:" + dateDiff);
                return result ;
            }
            catch (Exception ex)
            {
                FillErrorMsg("dt_users_class_verify_cold:" + ex);
                WriteErrorLog("dt_users_class_verify_cold:" + DateTime.Now.ToString(), ex.ToString() + "  補冷數據");
                return 1;
            }
        }

        /// <summary>
        /// 代理汇总前先判断下层级表的准确性  今天
        /// </summary>
        int dt_users_class_verify()
        {
            int result = 0;
            try
            {
                // int xx = Convert.ToInt32("sss0d5fg4");
                int count = 0;
                int reportcount = 0;
                int azurecount = 0;
                int reportmax = 0;
                int azuremax = 0;
                int reportmin = 0;
                int azuremin = 0;
                string date_now = System.DateTime.Now.ToString("yyyy-MM-dd");
                List<SqlParameter> LocalSqlParamter = new List<SqlParameter>()
                {
                    new SqlParameter("@addtime",SqlDbType.DateTime){ Value=date_now },
                    new SqlParameter("@flog1", SqlDbType.Int){ Value=1}
                };
                string str_report = "select count(id) count,isnull(min(id),0) min,isnull(max(id),0) max from dt_users_class_test where " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "addtime>=@addtime ";
                //获取报表中的层级最新的id
                var reporttable = SqlDbHelper.GetQuery(str_report, LocalSqlParamter.ToArray(),sql_agentOut);
                if (reporttable.Rows.Count > 0 && reporttable != null)
                {
                    reportcount = Convert.ToInt32(reporttable.Rows[0]["count"]);
                    reportmax = Convert.ToInt32(reporttable.Rows[0]["max"]);
                    reportmin = Convert.ToInt32(reporttable.Rows[0]["min"]);
                    //获取从库中层级最新的id
                    string str_congku = "select count(id) count,isnull(min(id),0) min,isnull(max(id),0) max  from dt_users_class where " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from dt_users where flog=@flog1) and addtime>=@addtime ";
                    var azurettable = SqlDbHelper.GetQuery(str_congku, LocalSqlParamter.ToArray(),reportConnStr);
                    azurecount = Convert.ToInt32(azurettable.Rows[0]["count"]);
                    azuremax = Convert.ToInt32(azurettable.Rows[0]["max"]);
                    azuremin = Convert.ToInt32(azurettable.Rows[0]["min"]);
                    if (reportmax >= azuremax && reportcount >= azurecount)//叢庫id<=報表id且叢庫數量<=報表數量表示同步數量相同
                    {
                        return result;
                    }
                    else if (azurecount > reportcount || azuremax > reportmax)//如報表有叢庫當天資料,並且報表數量和maxid都小於叢庫
                    {
                        string date_str = System.DateTime.Now.ToString("yyyy-MM-dd");
                        string date_str_e = System.DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                        //查看叢庫中id等於報表最大id內的資料比數
                        List<SqlParameter> UserClassSqlParamter = new List<SqlParameter>()
                        {
                            new SqlParameter("@date_now",SqlDbType.DateTime){ Value=date_now },
                            new SqlParameter("@reportmax", SqlDbType.Int){ Value=reportmax},
                            new SqlParameter("@flog1", SqlDbType.Int){ Value=1}
                        };
                        string report_count_str = "select count(id) count from dt_users_class_test where " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from srv_lnk_share.dafacloud.dbo.dt_users where flog=@flog1) and id<= @reportmax and addtime>=@date_now  ";
                        var check_report = SqlDbHelper.GetQuery(report_count_str, UserClassSqlParamter.ToArray(),sql_agentOut);
                        int check_reportcount = Convert.ToInt32(check_report.Rows[0]["count"]);
                        report_count_str = "select count(id) count from dt_users_class where " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from dt_users where flog=@flog1) and id<= @reportmax and addtime>=@date_now  ";
                        var check_azuret = SqlDbHelper.GetQuery(report_count_str, UserClassSqlParamter.ToArray(),reportConnStr);
                        int check_azurecount = Convert.ToInt32(check_azuret.Rows[0]["count"]);
                        bool issuccess = false;
                        string str_azure = "";
                        try
                        {
                            TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                            if (check_reportcount >= check_azurecount)//報表比數大於等於叢庫,表示只需要同步新資料
                            {
                                List<SqlParameter> UserClass_SqlParamter = new List<SqlParameter>()
                                {
                                    new SqlParameter("@date_str", SqlDbType.DateTime){Value=date_str },
                                    new SqlParameter("@date_str_e", SqlDbType.DateTime){Value=date_str_e },
                                    new SqlParameter("@reportmax", SqlDbType.Int){ Value=reportmax},
                                    new SqlParameter("@flog1", SqlDbType.Int){ Value=1}
                                };
                                str_azure = "select id,identityid,father_id,user_id,user_class,isagent,uclass,convert(varchar,addtime,121) as addtime from dt_users_class where " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from dt_users where flog=@flog1) and id>@reportmax and addtime >=@date_str and addtime<=@date_str_e ";
                                DataTable dt_azuret = SqlDbHelper.GetQuery(str_azure, UserClass_SqlParamter.ToArray(), reportConnStr);
                                RunSqlBulkCopy(dt_azuret, "dt_users_class_test", ref issuccess,sql_agentOut);
                                count = dt_azuret.Rows.Count;
                            }
                            else//需要整天重新檢查
                            {
                                count = Crue_data(azurecount, reportcount, azuremax, reportmax, date_str, date_str_e);
                            }
                            TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan ts = ts1.Subtract(ts2).Duration();
                            string dateDiff = ts.Seconds.ToString() + "秒";
                            FillMsg3("dt_user_class成功同步汇总" + count + "条数据,耗时:" + dateDiff);
                        }
                        catch (Exception ex)
                        {
                            FillErrorMsg("dt_users_class_verify:" + ex);
                            WriteErrorLog("dt_users_class_verify:" + DateTime.Now.ToString(), ex.ToString() + "\r\n" + "sql语句:" + str_azure + "\r\n" + "變數check_reportcount:" + check_reportcount + "\r\n" + "check_azurecount:" + check_azurecount);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                if (ex.Message != "未将对象引用设置到对象的实例。")
                {
                    FillErrorMsg("dt_users_class_verify:" + ex);
                    WriteErrorLog("dt_users_class_verify:連線失敗  " + DateTime.Now.ToString(), ex.ToString());
                }
                return 1;
            }
        }

        /// <summary>
        /// 批量插入
        /// </summary>
        private void RunSqlBulkCopy(DataTable dt, string tablename, ref bool isSuccess, string connectString)
        {
            if (dt.Rows.Count != 0)
            {
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connectString))
                {
                    sqlBulkCopy.DestinationTableName = tablename;
                    sqlBulkCopy.BulkCopyTimeout = 6000;
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (dt.Columns[i].ColumnName != "lockid")
                            sqlBulkCopy.ColumnMappings.Add(dt.Columns[i].ColumnName, dt.Columns[i].ColumnName);
                    }
                    try
                    {
                        isSuccess = true;
                        sqlBulkCopy.WriteToServer(dt);
                    }
                    catch (Exception ex)
                    {
                        isSuccess = false;
                        FillErrorMsg("批量插入" + tablename + ":" + ex);
                        WriteErrorLog("批量插入" + tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                    }
                }
            }
        }
        /// <summary>
        /// 補數據
        /// </summary>
        private int Crue_data(int azurecount, int reportcount, int azuremax, int reportmax, string date_s_str, string date_e_str)
        {
            //誤差比數
            int diff = azurecount - reportcount;
            int crue = 0;
            while (crue < diff || reportmax < azuremax)// 
            {
                //string date_s_str = date_s.ToString("yyyy-MM-dd");
                //string date_e_str = date_e.ToString("yyyy-MM-dd");
                List<SqlParameter> UserClass_SqlParamter = new List<SqlParameter>()
                {
                    new SqlParameter("@date_s_str", SqlDbType.DateTime){Value=date_s_str},
                    new SqlParameter("@date_e_str", SqlDbType.DateTime){Value=date_e_str},
                    new SqlParameter("@flog1", SqlDbType.Int){ Value=1}
                };
                string crue_str = "insert into dt_users_class_test (id,identityid,father_id,user_id,user_class,isagent,uclass,addtime)values";
                string str_azure = "select id,identityid,father_id,user_id,user_class,isagent,uclass,convert(varchar,addtime,121) as addtime from dt_users_class where " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from dt_users where flog=@flog1) and addtime >=@date_s_str and addtime<=@date_e_str OPTION (RECOMPILE)";
                string str_report = "select  id,identityid,father_id,user_id,user_class,isagent,uclass,convert(varchar,addtime,121) as addtime from dt_users_class_test where " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "addtime >=@date_s_str and addtime<=@date_e_str OPTION (RECOMPILE)";
                DataTable dt_azure = SqlDbHelper.GetQuery(str_azure, UserClass_SqlParamter.ToArray(),reportConnStr);
                DataTable dt_report = SqlDbHelper.GetQuery(str_report, UserClass_SqlParamter.ToArray(),sql_agentOut);
                try
                {
                    if (dt_azure.Rows.Count > dt_report.Rows.Count)
                    {
                        for (int r = 0; r < dt_azure.Rows.Count; r++)//
                        {
                            string azure_id = dt_azure.Rows[r]["id"].ToString();
                            if (dt_report.Select("id = " + azure_id + "").Count() == 0)
                            {
                                crue++;
                                if (reportmax < Convert.ToInt32(azure_id))
                                {
                                    reportmax = Convert.ToInt32(azure_id);
                                }
                                string identityid = dt_azure.Rows[r]["identityid"].ToString();
                                string father_id = dt_azure.Rows[r]["father_id"].ToString();
                                string user_id = dt_azure.Rows[r]["user_id"].ToString();
                                string user_class = dt_azure.Rows[r]["user_class"].ToString();
                                string isagent = dt_azure.Rows[r]["isagent"].ToString();
                                string uclass = dt_azure.Rows[r]["uclass"].ToString();
                                string addtime = dt_azure.Rows[r]["addtime"].ToString();
                                crue_str += " ('" + azure_id + "','" + identityid + "','" + father_id + "','" + user_id + "','" + user_class + "','" + isagent + "','" + uclass + "','" + addtime + "' ),";
                            }
                        }
                        if (crue > 0)
                        {
                            crue_str = crue_str.Substring(0, crue_str.Length - 1);
                            SqlDbHelper.ExecuteNonQuery(crue_str,sql_agentOut);
                        }
                    }
                    //  date_s = date_s.AddDays(-1);
                    //  date_e = date_e.AddDays(-1);
                }

                catch (Exception ex)
                {
                    FillErrorMsg("Crue_data:" + ex);
                    FillErrorMsg("sql语句:" + crue_str);
                    WriteErrorLog("Crue_data:" + DateTime.Now.ToString(), ex.ToString() + "\r\n" + "sql语句:" + crue_str + "\r\n" + "變數:crue" + crue + "\t" + "azurecount:" + azurecount + "\t" + "reportcount:" + reportcount + "\t" + "azuremax:" + azuremax + "\t" + "reportmax:" + reportmax + "\t" + "date_s_str:" + date_s_str + "\t" + "date_e_str:" + date_e_str);
                }
            }
            return crue;
        }

        private byte[] StringConvertByte(string sqlstring)
        {
            string stringFromSQL = sqlstring;
            List<byte> byteList = new List<byte>();

            string hexPart = stringFromSQL.Substring(2);
            for (int i = 0; i < hexPart.Length / 2; i++)
            {
                string hexNumber = hexPart.Substring(i * 2, 2);
                byteList.Add((byte)Convert.ToInt32(hexNumber, 16));
            }

            byte[] original = byteList.ToArray();
            return original;
        }
        #endregion

        #region richTextBox记录
        private delegate void RichBox1(string msg);
        private void FillMsg1(string msg)
        {
            if (richTextBox1.InvokeRequired)
            {
                RichBox1 rb = new RichBox1(FillMsg1);
                richTextBox1.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.AppendText(msg);
                    richTextBox1.AppendText("\r\n");
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.SelectionLength = 0;
                    richTextBox1.Focus();
                }
            }
        }

        private delegate void RichBox2(string msg);
        private void FillMsg2(string msg)
        {
            if (richTextBox2.InvokeRequired)
            {
                RichBox2 rb = new RichBox2(FillMsg2);
                richTextBox2.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox2.IsHandleCreated)
                {
                    richTextBox2.AppendText(msg);
                    richTextBox2.AppendText("\r\n");
                    richTextBox2.SelectionStart = richTextBox2.Text.Length;
                    richTextBox2.SelectionLength = 0;
                    richTextBox2.Focus();
                }
            }
        }

        private delegate void RichBox3(string msg);
        private void FillMsg3(string msg)
        {
            if (richTextBox3.InvokeRequired)
            {
                RichBox3 rb = new RichBox3(FillMsg3);
                richTextBox3.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox3.IsHandleCreated)
                {
                    richTextBox3.AppendText(msg);
                    richTextBox3.AppendText("\r\n");
                    richTextBox3.SelectionStart = richTextBox3.Text.Length;
                    richTextBox3.SelectionLength = 0;
                    richTextBox3.Focus();
                }
            }
        }

        private delegate void RichBoxErr(string msg);
        private void FillErrorMsg(string msg)
        {
            if (errorBox.InvokeRequired)
            {
                RichBoxErr rb = new RichBoxErr(FillErrorMsg);
                errorBox.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (errorBox.IsHandleCreated)
                {
                    errorBox.AppendText(msg);
                    errorBox.AppendText("\r\n");
                    errorBox.SelectionStart = errorBox.Text.Length;
                    errorBox.SelectionLength = 0;
                    errorBox.Focus();
                }
            }
        }
        #endregion

        #region 打印成功日志记录
        private object obj1 = new object();
        public void WriteLogData(string msgex)
        {
            lock (obj1)
            {
                if (!File.Exists(successLogPath))
                {
                    FileStream fs1 = new FileStream(successLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.Write(msgex);
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(successLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.Write(msgex);
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 错误日志记录
        private object obj = new object();
        public void WriteErrorLog(string msgex, string msgsql)
        {
            lock (obj)
            {
                if (!File.Exists(errorLogPath))
                {
                    FileStream fs1 = new FileStream(errorLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.WriteLine(msgex);
                    sw.WriteLine(msgsql);
                    sw.WriteLine();
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(errorLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.WriteLine(msgex);
                    sr.WriteLine(msgsql);
                    sr.WriteLine();
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 清理textbox
        private void clear_Elapse(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (System.DateTime.Now.ToString("mm") == "40")
                    ClearMsg();
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                timer_clear.Start();
            }
        }
        private delegate void RichBoxClear();
        private void ClearMsg()
        {
            if (richTextBox1.InvokeRequired & richTextBox2.InvokeRequired & richTextBox3.InvokeRequired & errorBox.InvokeRequired)
            {
                RichBoxClear rb = new RichBoxClear(ClearMsg);
                richTextBox1.Invoke(rb);
                richTextBox2.Invoke(rb);
                richTextBox3.Invoke(rb);
                errorBox.Invoke(rb);
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.Clear();
                    richTextBox2.Clear();
                    richTextBox3.Clear();
                    errorBox.Clear();
                }
            }
        }
        #endregion

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Dispose();
            Application.Exit();
            System.Environment.Exit(0);
        }

        private void compareSQLTime_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                string cmd = "select MAX(add_time) as add_time from dt_lottery_orders";
                DataTable tempTable = new DataTable();
                DateTime timeMain;
                DateTime timeReport;

                tempTable = SqlDbHelper.GetQuery(cmd, mainConnStr);
                timeMain = Convert.ToDateTime(tempTable.Rows[0]["add_time"]);
                tempTable = SqlDbHelper.GetQuery(cmd, reportConnStr);
                timeReport = Convert.ToDateTime(tempTable.Rows[0]["add_time"]);

                if (timeMain.Subtract(timeReport) >= TimeSpan.FromSeconds(60))
                {
                    if (reportDelay == false)
                    {
                        reportDelay = true;
                        WriteErrorLog("--------------------------\r\n報表從庫延遲", DateTime.Now.ToString() + "\r\n--------------------------");
                    }
                }
                else
                {
                    if (reportDelay == true)
                    {
                        WriteErrorLog("--------------------------\r\n報表從庫恢復", DateTime.Now.ToString() + "\r\n--------------------------");
                    }
                    reportDelay = false;
                }
            }
            catch (Exception ex)
            {
                reportDelay = true;
            }
            finally
            {
                compareDelayTimer.Start();
            }
        }
    }
}
